%{
  #include "Parser.h"
%}

%%
"import" { return IMPORT; } 
[a-zA-Z]+ { 
		yylval.sval = malloc(strlen(yytext));
		strncpy(yylval.sval, yytext, strlen(yytext));
		return(IDENTIFIER); 
	}
"." { return DOT; }
"*" { return STAR; }
";" { return SEMICOLON; }
[ \t]+ { /*printf("*scanned_space*");*/ /* ignore spaces*/ }
%%

int yywrap(){
  return 1;
}
