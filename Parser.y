%{
#include <stdio.h>
int lineNum = 1;
%}


%union
{
  char *sval;
};

%token <sval>  IDENTIFIER
%token IMPORT
%token DOT
%token STAR
%token SEMICOLON

%start ImportStatement 

%%

ImportStatement
	: IMPORT QualifiedName SemiColons
	| IMPORT QualifiedName DOT STAR SemiColons {printf("\nParsed import statement");}
	;

QualifiedName
	: QualifiedName DOT IDENTIFIER
	| IDENTIFIER {printf("\nParsed qualified name/identifier");}
	;

SemiColons
        : SemiColons SEMICOLON
	| SEMICOLON {printf("\nParsed semicolons");}
        ;

%%

int yyerror(char *s) {
  printf("yyerror : %s\n",s);
}

int main(void) {
  yyparse();
}
